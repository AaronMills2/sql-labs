DROP TABLE employee;
DROP TABLE car;
DROP TABLE lot;
DROP TABLE employees;
DROP TABLE departments;
DROP TABLE job_history;
DROP TABLE jobs;
DROP TABLE countries;

-- Write a SQL statement to create a table called countries. 
-- Include the columns: country_id, country_name and region_id.
CREATE TABLE countries (
			country_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY
		   ,country_name VARCHAR(60)
		   ,region_id INTEGER NOT NULL
			);

-- Write a SQL statement to create a table named jobs including 
-- columns job_id, job_title, min_salary, max_salary	
CREATE TABLE jobs (
			job_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY
		   ,job_title VARCHAR(60)
		   ,min_salary DECIMAL
		   ,max_salary DECIMAL
	);		
	
-- Write a SQL statement to create a table named job_history including 
-- columns employee_id, start_date, end_date, job_id and department_id	
CREATE TABLE job_history (
		employee_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY PRIMARY KEY
	   ,start_date DATE
	   ,end_date DATE
	   ,job_id INTEGER
	   ,department_id INTEGER
	   ,CONSTRAINT job_id_fk FOREIGN KEY (job_id) REFERENCES jobs(job_id)
	);	
	
-- Create a departments table with the following columns:
-- department_id DECIMAL(4,0) (Primary Key)
-- department_name VARCHAR(30)
-- manager_id DECIMAL(6,0) -- Default to 0 (Primary Key)
-- locaton_id DECIMAL(4,0) -- Default to null

CREATE TABLE departments (
	department_id INTEGER DEFAULT 0 NOT NULL UNIQUE
   ,department_name VARCHAR(30) UNIQUE
   ,manager_id INTEGER DEFAULT 0 UNIQUE
   ,location_id INTEGER DEFAULT NULL
   ,PRIMARY KEY (department_id, manager_id)
);


/*
 * Write a SQL statement to create a table employees including columns:
	employee_id 
	first_name 
	last_name 
	email 
	phone_number
	hire_date 
	job_id 
	salary 
	commission 
	manager_id 
	department_id
* Make sure that the employee_id column does not contain any duplicate values.
* Ensure that the foreign key columns combined by department_id and manager_id 
* columns contain only those unique combination values that exist in the departments table. 
 */

CREATE TABLE employees(
	employee_id INTEGER NOT NULL PRIMARY KEY
   ,first_name VARCHAR (40) NOT NULL
   ,last_name VARCHAR (40) NOT NULL
   ,email VARCHAR (80) NOT NULL
   ,phone_number VARCHAR (14) NOT NULL
   ,hire_date DATE
   ,job_id INTEGER
   ,salary DECIMAL
   ,commission DECIMAL
   ,manager_id INTEGER UNIQUE
   ,department_id INTEGER
   ,CONSTRAINT department_id_fk FOREIGN KEY (department_id) REFERENCES departments(department_id)
   ,CONSTRAINT manager_id_fk FOREIGN KEY (manager_id) REFERENCES departments(manager_id)
	);
/*
Employee
ID (Primary Key)
Name - String
Start Date - Date
Term Date - Date
Username - String
Birthdate - Date
Status - String that references the status �Active�, �Inactive� etc
*/
CREATE TABLE employee (
	id INTEGER PRIMARY KEY 
   ,name VARCHAR (255)
   ,start_date DATE
   ,term_date DATE
   ,username VARCHAR (255)
   ,birthdate DATE
   ,status VARCHAR (255)
);	
	
/*
Lot
ID (Primary Key) - Number	
Address - String
Manager - Foreign Key to Employee (via the ID)
 */

CREATE TABLE lot(
	id INTEGER PRIMARY KEY 
   ,address VARCHAR(255)
   ,manager_id INTEGER
   ,CONSTRAINT lot_manager_id_fk FOREIGN KEY (manager_id) REFERENCES employee(id)
);
	
/**
 * Car
ID (Primary Key), Make, Model, Year, Mileage, Image, Lot - Foreign Key
*/

CREATE TABLE car (
	id INTEGER PRIMARY KEY
   ,make VARCHAR(30)
   ,model VARCHAR(50)
   ,"year" INTEGER
   ,mileage DECIMAL(10, 0)
   ,image VARCHAR(255)
   ,lot INTEGER
   ,CONSTRAINT lot_fk FOREIGN KEY (lot) REFERENCES lot(id)
);




	