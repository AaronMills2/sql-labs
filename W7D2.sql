/*
Continue working with your �lot� problem from yesterday.
Insert an employee named Frank who is currently active into your employee table. 
Frank started July 25, 1992.
*/
INSERT INTO EMPLOYEE 
VALUES(1, 'Frank', '1992-07-25', null, null, null, 'Active');
/*
Insert an employee named Susan who is currently active into your employee table. 
Susan started May 1, 2002.
*/
INSERT INTO EMPLOYEE 
VALUES(2, 'Susan', '2002-05-05', null, null, null, 'Active');
/*
Insert an employee named John who is a past employee into your employee table. 
John started February 12, 1998. John left August 23, 2005.
*/
INSERT INTO EMPLOYEE 
VALUES(3, 'John', '1998-02-12', '2005-08-23', null, null, 'Inactive');
/*
Insert an employee named Allen who is currently active into your employee table. 
Allen started October 31, 2010.
*/
INSERT INTO EMPLOYEE 
VALUES(4, 'Allen', '2010-10-31', null, null, null, 'Active');
/*
Create a Lot at 123 Fake Street. Make Frank the manager of this lot.
*/

INSERT INTO lot (id, address, manager_id)
VALUES(1, '123 Fake Street', (
	SELECT id FROM employee WHERE name LIKE 'Frank'));
/*
Create a second Lot at 394 Fun Street. Make Susan the manager of this lot.
*/
INSERT INTO lot (id, address, manager_id)
VALUES (2, '394 Fun Street', (
	SELECT id FROM employee WHERE name LIKE 'Susan'));
/*
Add a 2008 Toyota Corolla to the Car table with 100000 miles. This car is at the Fake Street lot.
*/
INSERT INTO car (lot, image, mileage, "year", model, make, id)
VALUES ((SELECT id FROM lot WHERE address LIKE '%Fake Street'), null, 100000, 2008, 'Corolla', 'Toyota', 1)
/*
Add a 2015 Mazda 3 to the Car Table with 45000 miles. This car is at the Fake Street lot.
*/
INSERT INTO car (lot, image, mileage, "year", model, make, id)
VALUES ((SELECT id FROM lot WHERE address LIKE '%Fake Street'), null, 45000, 2008, '3', 'Mazda', 2)
/*
Add a 2010 Ford Taurus to the Car Table with 124000 miles. This car is at the Fake Street lot.
*/
INSERT INTO car (lot, image, mileage, "year", model, make, id)
VALUES ((SELECT id FROM lot WHERE address LIKE '%Fake Street'), null, 124000, 2010, 'Taurus', 'Ford', 3)
/*
Add a 2008 Chrysler PT Cruiser to the Car table with 88000 miles. This car is at the Fun Street lot.
*/
INSERT INTO car (lot, image, mileage, "year", model, make, id)
VALUES ((SELECT id FROM lot WHERE address LIKE '%Fun Street'), null, 88000, 2008, 'PT Cruser', 'Chrysler', 4)
/*
Add a 2011 BMW M3 to the Car Table with 95000 miles. This car is at the Fun Street lot.
*/
INSERT INTO car (lot, image, mileage, "year", model, make, id)
VALUES ((SELECT id FROM lot WHERE address LIKE '%Fun Street'), null, 95000, 2011, 'M3', 'BMW', 5)
/*
Add a 1999 Toyota Camry to the car table with 200000 miles. This car is at the Fun Street lot. 
*/
INSERT INTO car (lot, image, mileage, "year", model, make, id)
VALUES ((SELECT id FROM lot WHERE address LIKE '%Fun Street'), null, 200000, 1999, 'Camry', 'Toyota', 6)
/*
Selects
Select all the active employees.
*/
SELECT *
FROM employee
WHERE status LIKE 'Active';
/*
Select employees who started before the year 2000.
*/
SELECT *
FROM employee
WHERE start_date < '2000-01-01';
/*
Select all Toyota cars that have less than 200000 miles.
*/
SELECT *
FROM car
WHERE CAR.MILEAGE < 200000 AND 
			 CAR.MAKE LIKE 'Toyota';
/*
Select all vehicles made after 2005 with more than 80000 miles.
*/
SELECT * 
FROM car
WHERE "year" > 2005 AND
		     mileage > 80000;