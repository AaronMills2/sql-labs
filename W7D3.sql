DROP TABLE registration;

CREATE TABLE registration (
	id INTEGER
   ,admitted_date DATE
   ,status VARCHAR(25)
   ,student_id INTEGER
   ,PRIMARY KEY (id)
	);
	
INSERT INTO registration (id, admitted_date, status, student_id)
VALUES (10100, '2018-01-06', 'ADMITTED', 363)
			 ,(10101, '2018-01-06', 'ADMITTED', 128)
			 ,(10102, '1970-01-01', 'APPLICANT', 181)
			 ,(10103, '1970-01-01', 'REGISTERED', 121)
			 ,(10104, '1970-01-01', 'REGISTERED', 114)
			 ,(10106, '1970-01-01', 'APPLICANT', 278)
			 ,(10120, '2018-01-07', 'ADMITTED', 115)
			 ,(10122, '2018-05-05', 'ADMITTED', 350)
			 ,(10123, '1970-01-01', 'REGISTERED', 103);

SELECT * 
FROM registration
WHERE UPPER(status) NOT LIKE 'REGISTERED'
ORDER BY admitted_date, id
FETCH FIRST 5 ROWS ONLY;

-- select distinct over multiple cols


	
